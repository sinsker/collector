var fs  = require('fs');
var config  = require('../config/collector');
var common  = require('../modules/common.js');

var CollectorOptionManager = {

    //options 저장 경로
    path :  __dirname + '/../options/',

    //가져올 JSON 값
    options : config.mms_config,

    //기본 저장 dataset
    defaultDataset :config.default_dataset,

    //사용자 정의의 데이터를 체크 합니다.
    datasetFilter : function(dataset, callback){

        var datasetList = this.getOption('dataset');
        var options = datasetList ? JSON.parse(datasetList) : [];
        options = this.defaultDataset.concat(options);

        for(var key in dataset){
            if(options.indexOf(key) === -1){
                delete dataset[key];
            }

            dataset[key] = typeof dataset[key] == 'undefined' || !dataset[key] ? '' :  dataset[key];

            try{
                dataset[key] = decodeURIComponent(dataset[key]);
            }catch(e){
                //URL Error
            }

        }

        if (callback && (typeof(callback) === "function")) {
            callback(dataset);
        }
        return dataset;
    },

    //SNS URL 필터
    snsFilter : function(referrer){

        var snsList = this.getOption('sns');
        var blankSNS = '';

        if(snsList == false || !referrer ) return blankSNS;
        var domain = common.getDomain(referrer,true);
        snsList = JSON.parse(snsList);
        for(var key in snsList){
            var sns = snsList[key];
            for(var name in sns){
                if(sns[name].indexOf(domain) !== -1){
                    return name;
                }
            }
        }
        return blankSNS;
    },

    //URL 필터
    URLFilter : function(URL){

        var filterData = false;

        if(!URL) return filterData;

        var domain = common.getDomain(URL);


        if(domain.indexOf('cafe') !== -1){
            filterData = {
                'type':'cafe_referer',
                'referer':this.cafeURLFilter(domain, URL)
            };
        }else if(domain.indexOf('blog') !== -1 || domain.indexOf('tistory') !== -1){

            filterData = {
                'type':'blog_referer',
                'referer':this.blogURLFilter(domain, URL)
            }
        }
        return filterData;
    },

    //카페 URL 필터
    cafeURLFilter : function(domain, originURL){

        var params = common.getUrlParams(originURL);

        return this.patternMatchConversion(domain, params, config.cafe_pattern, this.cafeUniqueKeyReplace) || originURL;
    },

    //블로그 URL 필터
    blogURLFilter : function(domain, originURL){

        var params = common.getUrlParams(originURL);

        return this.patternMatchConversion(domain, params, config.blog_pattern) || originURL;
    },

    //URL 패턴에 맞게 데이터 변경 변경
    patternMatchConversion: function(domain, params, pattern, filterSuccessHook){

        var conversionURL = '';
        var isFilter = false;
        for(var i in pattern){
            var values = pattern[i];
            for(var key in values){
                if(domain.indexOf(key) !== -1 && isFilter == false){
                    var paramPattern = values[key]['pattern'];
                    var template     = values[key]['template'];

                    for(var paramKey in paramPattern){
                        if(params.hasOwnProperty(paramKey)){
                            template = template.replace(paramPattern[paramKey],params[paramKey]);
                            isFilter = true;
                        }else{
                            template = template.replace(paramPattern[paramKey],'');
                        }
                    }
                    conversionURL = template;
                }
            }
        }

        if(isFilter){
            if(conversionURL.charAt(conversionURL.length - 1) == '/') conversionURL = conversionURL.slice(0,-1);
            conversionURL = domain + '/' + conversionURL;

            //filter Success Hook
            if(filterSuccessHook && typeof filterSuccessHook === 'function'){
                conversionURL = filterSuccessHook(conversionURL, this);
            }

        }else{
            conversionURL = '';
        }

        return conversionURL;
    },

    //카페의 고유 idx 를 원래 이름으로 치환한다.
    cafeUniqueKeyReplace : function(conversionURL, that){

        var matchedUniqueKeyObject = {};
        var cafeUniqueKeyList = that.getOption('cafe');

        if(cafeUniqueKeyList == false) return conversionURL;

        var options = JSON.parse(cafeUniqueKeyList)[0];

        for(var domain in options){
            if(conversionURL.indexOf(domain) !== -1){
                matchedUniqueKeyObject = options[domain];
            }
        }

        var cafeUniqueKey = conversionURL.split("/")[3];

        if(matchedUniqueKeyObject.hasOwnProperty(cafeUniqueKey)){
            conversionURL =conversionURL.replace(cafeUniqueKey,matchedUniqueKeyObject[cafeUniqueKey]);
        }

        return conversionURL;
    },

    //예외 useragent // 크롤링 방지
    exceptionUseragent : function(user_agent, callback) {

        var is_execption = false;
        var exception = 'BingPreview';
        if(user_agent.indexOf(exception) !== -1 ){
            is_execption = true;
        }

        if (callback && (typeof(callback) === "function")) {
            callback(options);
        }

        return is_execption;
    },

    //예외 IP 체크
    exceptionIP: function(ip, callback) {

        var is_execption = false;
        var exceptionIpList = this.getOption('ip');

        if(exceptionIpList == false) return is_execption;

        var options = JSON.parse(exceptionIpList);

        if(options.indexOf(ip) !== -1 ){
            is_execption = true;
        }

        if (callback && (typeof(callback) === "function")) {
            callback(options);
        }

        return is_execption;
    },


    //=========================== 기본 파일 시스템 ========================================

    //옵션 값 가져오기
    getOption : function(type){

        if( this.options.indexOf(type) === -1 ) return false;

        this.createOptionFile(type);

        var std = fs.readFileSync(this.path + type +'.option', 'utf8');
        if(!std) fs.writeFileSync(this.path + type +'.option','[]','utf8');

        return std;
    },

    //옵션 값 초기화
    initOptions : function(type, options){

        this.createOptionFile(type);

        var option = this.getOption(type) || [];

        if(option === false) return false;

        var result =  fs.writeFileSync(this.path + type +'.option', options, 'utf8');

        return true;
    },
    

    //옵션 기본 파일 생성하기
    createOptionFile :function(type){

        var file = this.path + type +'.option';

        if(!fs.existsSync(file)){

            fs.writeFileSync(this.path + type +'.option','[]','utf8');
        }
    }

};



module.exports = CollectorOptionManager;