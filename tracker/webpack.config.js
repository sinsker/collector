// webpack.config.js
var webpack = require('webpack');
var path = require('path');
//var es3ifyLoader = require("es3ify-loader");

module.exports = {
    entry: ['/var/www/html/producer/tracker/src/main'],
    output: {
        path:  path.join(__dirname, '../public/'),
        filename: 'HSA.tracker.js'
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({ //압축
           /* compressor: {
                warnings: false
            },*/
            mangle: {
                screw_ie8: false
                // mangle options, if any
            },
            mangleProperties: {
                screw_ie8: false
                //ignore_quoted: true,
            },
            compress: {
                screw_ie8: false
                //properties: false
            },
            output: {
                screw_ie8: false
            }
        }),
    ]
//    module: {
//        loaders: [{
//            test: /\.js$/,
//            loader: 'babel',
//            exclude: /(node_modules|tracker)/,
//
//            query: {
//                plugins: ['transform-es3-property-literals'],
//               presets: [['es2015', {"loose": true}]]
//            }
//        }]
//    }

};

