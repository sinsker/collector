
module.exports = {


    //// Marketing Manage Server 와 통신할 Config 규약 ////
    /* domain : 수집할 사이트 도메인,
     * ip : 예외 처리할 IP address
     * dataset : 기본 데이터 이외에 추가될 데이터 */
    mms_config : [
        'exip',
        'dataset',
        'sns',
        'cafe',
        'event'
    ],

    //// default_dataset ////
    /* 기본적으로 collector 되는 데이터 */
    default_dataset : [
        'page_url',
        'event_type',

        'event_id',
        'event_name',

        'site_id',
        'http_uri',
        'http_host',
        'http_referer',
        'first_visit',

        'hid',
        'script_version',

        'uid',
        'uidx',

        'session_id',
        'first_session',
        'request_id',
        'browser_engine',
        'user_ip',
        'user_agent',
        'user_device',
        'os_version',
        'timestamp',
        'time',
        'browser_version',
        'http_query',
        'os_name',
        'browser_name',
        'datetime',
        'page_title',

        'location_city',
        'location_region',
        'location_country',

        'search_terms',
        'search_engine',
        'search_medium',

        /* 캠페인 관련 */
        'campaign_id',
        'utm_source',
        'utm_campaign',
        'utm_medium',
        'utm_content',
        'utm_term',

        /* 클릭 이벤트 관련*/
        'dom_element_name',
        'dom_element_value',
        'dom_element_id',
        'dom_element_class',
        'dom_element_tag',
        'target_url',
        'dom_element_text',
        'dom_element_x',
        'dom_element_y',

        /*결제 관련 데이터*/
        'order_id',
        'order_name',
        'order_price',
        'order_revenue',
        'order_detail_cd',
        'order_detail_name',
        'order_detail_price',
        'order_detail_revenue',


        /*디버깅 용도*/
        'isSafari',
        'g'

    ],

    'cafe_pattern':[
        {'cafe.naver.com': {'pattern': {'clubid':'$1','articleid':'$2'},'template':'$1/$2' } },
        {'cafe.naver.com': {'pattern': {'search.clubid':'$1','search.articleid':'$2'},'template':'$1/$2' } }
    ],
    'blog_pattern':[
        {'blog.naver.com': {'pattern': {'blogId':'$1','logNo':'$2'},'template':'$1/$2'} }
    ]
};
