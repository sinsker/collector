
var Common = {

    getClientIP : function(req){

        var ipAddress;

        if(!!req.hasOwnProperty('sessionID')){
            ipAddress = req.headers['x-forwarded-for'];
        } else{
            if(!ipAddress){
                var forwardedIpsStr = req.header('x-forwarded-for');

                if(forwardedIpsStr){
                    var forwardedIps = forwardedIpsStr.split(',');
                    ipAddress = forwardedIps[0];
                }
                if(!ipAddress){
                    ipAddress = req.connection.remoteAddress;
                }
            }
        }
        return ipAddress;
    },


    getDateTime: function(date) {
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min  = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec  = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var msec = date.getMilliseconds();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day  = date.getDate();
        day = (day < 10 ? "0" : "") + day;

        return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + "." + msec;
    },

    getUrlParams: function (url) {
        var request = {};
        var pairs = url.substring(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < pairs.length; i++) {
            if(!pairs[i])
                continue;
            var pair = pairs[i].split('=');
            request[(pair[0])] = (pair[1]);
        }
        return request;
    },

    getParamsToUrl: function(array){
        var pairs = [];
        for (var key in array)
            if (array.hasOwnProperty(key))

                pairs.push((key) + '=' + (array[key]));
        return pairs.join('&');

    },

    getDomain : function(url, flag ){
        var pattern =  /https?:\/\/([a-z0-9-_.]*)/i;
        domain = url.match(pattern)[0];
        if(flag == true){
           var domainSplit = domain.split('/');
           return domainSplit[2];
        }
        return  domain;
    },

    apiResultMassage:function(response){
        return  response;
    },

   stripslashes :function(str){
        return (str + '')
            .replace(/\\(.?)/g, function (s, n1) {
                switch (n1) {
                    case '\\':
                        return '\\';
                    case '0':
                        return '\u0000';
                    case '':
                        return '';
                    default:
                        return n1;
                }
            })
    },

    addslashes : function(str){
        return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    },


    clone : function (obj) {
        if (obj === null || typeof(obj) !== 'object')  return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = obj[attr];
            }
        }
        return copy;
    }

};



module.exports = Common;