var CollectorOptionManager  = require('../modules/collectorOptionManager.js');
var fs  = require('fs');
var common  = require('../modules/common.js');
var config  = require('../config/collector');

var CollectionProcesser = {

    collection: '',

    exceptionMassage: {},
    exceptionHandler: [],
    filterHandler: [],

    //클릭이벤트 발생 여부
    clickTriggerFlag : false,

    eventActionEventCollection: [],

    //데이터 초기화
    initialization: function (collection, callback) {
        this.clickTriggerFlag = false;
        this.eventActionEventCollection = [];
        if (Object.keys(collection).length === 0) {
            callback('데이터가 존재 하지 않습니다.');
            return false;
        }
        this.collection = collection;
    },

    addCollection: function (key, value) {
        this.collection[key] = (value == '' ? null : value);
    },

    getCollection: function (key) {
        if (!this.isCollection(key)) return '';
        return this.collection[key];
    },

    deleteCollection: function (key) {
        if (Array.isArray(key)) {
            for (i in key) {
                if (this.isCollection(key[i])) {
                    delete this.collection[key[i]];
                }
            }
        } else {
            if (this.isCollection(key)) {
                delete this.collection[key];
            }
        }
    },

    isCollection: function (key) {
        return this.collection.hasOwnProperty(key);
    },

    data: function () {
        return this.collection;
    },

    //페이지뷰 이벤트
    pageviewEventList:function(){

        if(this.eventActionEventCollection.length === 0 ) return false;

        return this.eventActionEventCollection;
    },

    //실행
    run: function () {

        this.filter();

        this.eventActionStatusCheck();
    },


    //예외 체크 추가
    exception: function (exception) {
        if (typeof exception === 'function') this.exceptionHandler.push(exception(this.collection));
        return this;
    },

    //데이터 필터
    filter: function () {
        //dataFiler 필터
        collection = CollectorOptionManager.datasetFilter(this.collection);

        //sns 필터
        var sns = CollectorOptionManager.snsFilter(this.collection['http_referer']);
        this.addCollection('sns', sns);

        //cafe & blog referer 필터
        var filterData = CollectorOptionManager.URLFilter(this.collection['http_referer']);
        if (filterData !== false) {
            this.addCollection(filterData.type, filterData.referer);
        }
    },

    //이벤트 발생 여부 체크
    eventActionStatusCheck: function () {

        var clickActionQueue = [];
        var pageviewActionQueue = [];

        var eventList = JSON.parse(CollectorOptionManager.getOption('event'));
        if (eventList.length === 0) return false;

        eventList = eventList[0];
        if(!eventList.hasOwnProperty(this.getCollection('site_id'))) return false;

        eventList = eventList[this.getCollection('site_id')];

        for (i in eventList) {
            if (eventList[i]['type'] === "CLICK") {
                clickActionQueue.push(eventList[i]);
            } else if (eventList[i]['type'] === "PAGEVIEW") {
                pageviewActionQueue.push(eventList[i]);
            }
        }

        if(this.getCollection('event_type') === 'event'){
            this.clickTriggerFlag = this.eventActionClickTrigger(clickActionQueue);
        }else if(this.getCollection('event_type') === 'pageview'){
            this.eventActionPageviewTrigger(pageviewActionQueue);
        }
    },

    //클릭 이벤트 체크
    eventActionClickTrigger: function (queue) {

        if(queue.length === 0) return false;
        var clickTriggerFlag = false;

        for(i in queue){
            if(this.getCollection('event_id') && this.getCollection('event_id') === queue[i]['id']){
                this.addCollection('event_name',queue[i]['name']);
                return true;
            }
        }

        return clickTriggerFlag;
    },

    //페이지뷰 이벤트 체크
    eventActionPageviewTrigger: function(queue){

        if(queue.length === 0) return false;



        for(i in queue){

            var url_pattern  = queue[i]['url_pattern'];
            var url_ignore  = queue[i]['url_ignore'];
            var url_type  = queue[i]['url_type'];

            var URL =  this.getCollection('http_uri') + this.getCollection('http_query');

            if(url_ignore === true){
                URL = URL.toLowerCase();
                url_pattern = url_pattern.toLowerCase();
            }


            if( url_type === 'same'){
                if(URL === url_pattern){
                    var temp = common.clone(this.data());
                    temp['event_type'] = 'event';
                    temp['event_id'] = queue[i]['id'];
                    temp['event_name'] = queue[i]['name'];
                    this.eventActionEventCollection.push(temp);
                }
            }else{
                var matchPattern = new RegExp(url_pattern);
                if (matchPattern.test(URL)){
                    var temp = common.clone(this.data());
                    temp['event_type'] = 'event';
                    temp['event_id'] = queue[i]['id'];
                    temp['event_name'] = queue[i]['name'];
                    this.eventActionEventCollection.push(temp);
                }
            }
        }

    }


};


module.exports = CollectionProcesser;