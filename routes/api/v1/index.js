var express = require('express');
var router  = express.Router();

var common  = require('../../../modules/common.js');
var api  = require('../../../config/api');

//IP 허용 체크
router.use('/', function(req, res, next){

    console.log(req.header('origin'));

    if(typeof req.header('origin') !== 'undefined'){

        var remoteHost = req.header('origin').split('/')[2];

        if( api.allowHost.indexOf(remoteHost) === -1 ){
            return res.status(401).json({'header':JSON.stringify(remoteHost), 'result':'fail', 'massage': '인증된 클라이언트가 아닙니다.'});
        }
    }

    next();
});

router.use('/dataset',require('./dataset'));
router.use('/exip',require('./exip'));
router.use('/sns',require('./sns'));
router.use('/cafe',require('./cafe'));
router.use('/event',require('./event'));

module.exports = router;
