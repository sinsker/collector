var express = require('express');
var router  = express.Router();
var collectorOptionManager  = require('../../../modules/collectorOptionManager.js');

//옵션 초기화 저장
router.post('/init', function(req, res){

    var params = req.body['option'];

    console.log(params);

    if (Object.keys(params).length === 0) {
        return res.status(404).json({'result':'fail','massage':'invalid request data'});
    }

    var result = collectorOptionManager.initOptions('dataset', params);

    if(result === false){
        return res.status(404).json({'result':'fail','massage':'not is option'});
    }

    return res.status(200).json({'result':'success','massage': 'dataset' + ' data init'});

});


//옵션 불러오기
router.get('/', function(req, res){

    var option = collectorOptionManager.getOption('dataset');

    if(option === false){
        return res.status(404).json({'result':'fail','massage':'not is option'});
    }

    return res.send(option);
});


module.exports = router;
