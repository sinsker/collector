const  redis = require("redis");



var RedisBuffer = function(options){

    this.options = Object.assign({}, options);

    this.connect = redis.createClient(this.options.port, this.options.host);

    this.connect.on('connect', function(e) {
        console.log('redis connected...' )
    });

    this.connect.on("error", function (err) {
        console.log("Error " + err);
    });

};


RedisBuffer.prototype = {

    connect : null,
    options : {},

    client : function(){
        return this.connect;
    },

    set : function(key, value){
        this.client().lpush([key, value]);

        return this;
    },

    get : function(key){
        return this.client().get(key);
    },

    close : function(){
        this.client().quit();
    }

};


module.exports = RedisBuffer;
