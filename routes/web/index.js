var express = require('express');
var common  = require('../../modules/common.js');
var CollectorOptionManager  = require('../../modules/collectorOptionManager.js');
var Processer = require('../../modules/collectionProcesser.js');
var redisBuffer = require('../../modules/buffer/redis_buffer.js');

var router  = express.Router(),
    p3p = require('p3p');

/* CORS 저장소 역할 */
router.get('/storage',p3p(p3p.recommended), function(req, res){

    var CrossStorageHub = require('cross-storage').CrossStorageHub;
    return res.render('hub', { 'CrossStorageHub' : CrossStorageHub } );
});

/* collect tracker 저장소에 전송 */
router.all('/send', function (req, res) {



    var param = {};

    if(req.method == 'GET'){
         param = req.query;
    }else{
         param = JSON.parse(JSON.stringify(req.body));
    }

    var date = new Date();

    //var param = req.query;
    Processer.initialization(param, function(error){
        return res.status(404).json({'result':'fail','massage':'데이터가 존재하지 않습니다.'});
    });

    /* 회원 아이피 얻기 */
    Processer.addCollection('user_ip'  ,common.getClientIP(req));
    Processer.addCollection('datetime' ,common.getDateTime(date));
    Processer.addCollection('timestamp',date.getTime());
    Processer.addCollection('time'     ,Math.floor(date.getTime()/1000));

    var geo = require("geoip-lite").lookup( Processer.getCollection('user_ip'));
    var device = require('device');


    if(geo != null){
        Processer.addCollection('location_city',    geo['city']);
        Processer.addCollection('location_region',  geo['region']);
        Processer.addCollection('location_country', geo['country']);
    }

    var userAgentCheck = device(Processer.getCollection('user_agent'),{carUserAgentDeviceType : 'desktop'});
    var userDevice = 'pc';

    if(userAgentCheck.type === 'tablet'){
        userDevice = 'tablet';
    }else if(userAgentCheck.type === 'phone'){
        userDevice = 'phone';
    }
    Processer.addCollection('user_device', userDevice);

    if((!Processer.getCollection('user_ip') || !Processer.getCollection('http_host')) === true){
        return res.status(403).json({'result':'fail', 'massage': 'ERROR: User IP 혹은 HTTP HOST가 올바르지 않습니다.'});
    }

    if( CollectorOptionManager.exceptionUseragent(Processer.getCollection('user_agent')) === true || userAgentCheck.type === 'bot'){
        return res.status(403).json({'result':'fail', 'massage': 'ERROR:  인증된 유저 UserAgent가 아닙니다.'});
    }

    if(CollectorOptionManager.exceptionIP(Processer.getCollection('user_ip')) === true){
        return res.status(403).json({'result':'fail', 'massage': 'ERROR: 예외 아이피로 차단 되었습니다.'});
    }

    // 데이터 가공 및 필터 처리 //
    Processer.run();

    if(Processer.getCollection('event_type') === 'event' && Processer.clickTriggerFlag == false){
        return res.status(404).json({'result':'fail', 'massage': '존재하지 않는 이벤트 입니다.'});
    }

    //버퍼 생성
    buffer = new redisBuffer(require('../../config/redis.js'));

    var pageviewEventList = Processer.pageviewEventList();
    //이벤트 삽입
    if( pageviewEventList !== false){
        for(i in pageviewEventList){
            buffer.set('pageview', JSON.stringify(pageviewEventList[i]));
        }
    }

    var table = Processer.getCollection('event_type');
    if(['event','order','join'].indexOf(table) !== -1){
        table = 'pageview';
    }

    /* Buffer Config 로딩 */
    buffer.set(table, JSON.stringify(Processer.data()));

    console.log('tracking',Processer.data());

    /* 메모리 누수 방지*/
    buffer.close();
    buffer = null;
    param  = null;

    res.status(200).end();
});

module.exports = router;