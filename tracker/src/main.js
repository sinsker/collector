var hsa = require('./../vendor/tracker.js');

(function(w) {

    "use strict";

    var bodyLoad = false;

    if ( hsa.util.isBrowserTrackable() && typeof HS.q !== 'undefined'  ) {
        if ( hsa.util.is_array(HS.q) && HS.q.length > 0 ) {

            tracker();

            w.HS_event = function (key, datas) {
                if (key) {
                    datas = datas || {};

                    var action = new hsa.event();
                    action.setEventType("event");
                    action.set('event_id', key);

                    if (hsa.util.is_array(datas) && datas.length > 0) {
                        for (var key in datas) {
                            action.set(key, datas[key]);
                        }
                    }
                    hsa.track.trackEvent(action);
                }
            };
        }
    }

    function tracker(){

        if(document.body === null){
            setTimeout(function(){
                tracker();
            },100);

        }else{
            hsa.track = new hsa.tracker(); //
            hsa.queue = new hsa.Queue();
            hsa.queue.loadQueueStack(w.HS.q).process();
        }
    }

})(window);


module.exports = hsa;