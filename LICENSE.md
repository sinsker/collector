### Collecter Server
 
 ubuntu16.04 + nginx + nodejs 
 
#### 1. tracker.js
 
##### 1.1 간편 예제

```javascript 
(function(a,b,c,d,e){a['hksaObj']=e;a[e]=a[e]||function(){(a[e].q=a[e].q||[]).push(arguments)},a[e].l=1*new Date;s=b.createElement(c);x=b.getElementsByTagName(c)[0];s.async=1;s.src=d;x.parentNode.insertBefore(s,x)})(window,document,'script','//collector.hackers.com/HSA.tracker.js','HS');
```

```javascript
HS('create','site','hackers'); 

HS('set','uid','test@gmail.com');
HS('set','level', 'A' );

HS('send','pageview'); 
HS('send','clicks'); 
```


##### 1.2 사이트 적용 방법

* \<head\> 영역에 축소된 스크립트를 붙입니다.

* 사이트 고유 코드를 작성하여 넣습니다.
```javascript
HS('create','site','{site_code}'); 
````

* 사이트 환경변수를 원하는 형태로 변경합니다. 

```javascript
HS('env','{env_key}','{env_value}'); 
```

ex) 캠페인 세션 유지시간을 10분으로 변경 [ `default: 1800` ] 
```javascript
HS('env','campaign',(60*30) ); 
```
 
* 사이트 고유한 데이터를 추가로 수집하려면  set 명령어를 통해 입력을 합니다.
 
```javascript
HS('set','{key}', '{value}' );
```

* 전송할 이벤트 send 명령어로 선언합니다. [ `pageview`, `clickes` ]

```javascript 
HS('send','pageview');
HS('send','clicks');
```
 
 
#### 2. Producer API 
 nodejs 로 구성된 api 를 통해 cross-domain 처리 및 수집을 한 데이터를 redis/fluentd 혹은 파일로 buffer 를 쌓습니다.
   
 
 