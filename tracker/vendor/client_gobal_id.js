var CrossStorageClient = require('./cors/client');
var HSA_ENV = require('./tracker.env');
require('es6-promise').polyfill();

var ClientGlobalID = {

    gid : 'client_global',

    baseUrl: HSA_ENV.apiEndPoint(),
    hubApi : '/tracker/storage',

    storage : false,

    storageLoad : function(){

        if(this.storage == false) {
            this.storage = new CrossStorageClient(this.baseUrl + this.hubApi, {
                timeout: 3000,
                frameId: '_storageFrm'
            });
        }
    },

    get : function(key,callback){

        var that = this;
        var gid = '';

        that.storageLoad();

        that.storage.onConnect().then(function () {
            return that.storage.get(key);
        }).catch(function(err) {

        }).then(callback);

    },

    set : function(key, value, expires){

        var that = this;

        that.storageLoad();

        this.storage.onConnect().then(function() {
            return that.storage.set(key, value, expires);
        }).catch(function(err) {

        });

    }

};




module.exports = ClientGlobalID;


