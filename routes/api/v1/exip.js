var express = require('express');
var router  = express.Router();
var collectorOptionManager  = require('../../../modules/collectorOptionManager.js');

//옵션 초기화 저장
router.post('/init', function(req, res){

    var params = req.body['option'];

    console.log(params);

    if (Object.keys(params).length === 0 ) {
        return res.status(404).json({'result':'fail','massage':'invalid request data'});
    }

    var result = collectorOptionManager.initOptions('exip', params);

    if(result === false){
        return res.status(404).json({'result':'fail','massage':'not is option'});
    }

    return res.json({'result':'success','massage': 'exip' + ' data init'});

});


//옵션 전체 불러오기
router.get('/', function(req, res){

    var option = collectorOptionManager.getOption('exip');

    if(option === false){
        return res.status(404).json({'result':'fail','massage':'not is option'});
    }

    return res.send(option);
});

module.exports = router;
