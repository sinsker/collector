var express = require('express');
var router  = express.Router();
var collectorOptionManager  = require('../../../modules/collectorOptionManager.js');

var optionName = 'event';

//옵션 전체 불러오기
router.get('/', function(req, res){

    var option = JSON.parse(collectorOptionManager.getOption(optionName));

    return res.send(option);
});

//옵션 초기화 저장
router.post('/init', function(req, res){

    var params = req.body['option'];

    if (Object.keys(params).length === 0 ) {
        return res.status(404).json({'result':'fail','massage':'값이 넘어오지 않았습니다.'});
    }

    var result = collectorOptionManager.initOptions(optionName, params);

    if(result === false){
        return res.status(404).json({'result':'fail','massage':'not is option'});
    }

    return res.json({'result':'success','massage': 'exip' + ' data init'});

});

//옵션 저장&수정
router.post('/', function(req, res){

    var params = JSON.parse(JSON.stringify(req.body));

    if (Object.keys(params).length === 0) return res.status(404).json({'result':'fail','massage':'넘어온 데이터가 존재하지 않습니다.'});
    if(!params.hasOwnProperty('site') || !params['site']) return res.status(404).json({'result':'fail','massage':'  사이트 값이 넘어오지 않았습니다.'});
    if(!params.hasOwnProperty('id')   || !params['id']) return res.status(404).json({'result':'fail','massage':' 이벤트 ID가 넘어오지 않았습니다.'});
    if(!params.hasOwnProperty('name') || !params['name']) return res.status(404).json({'result':'fail','massage':'이벤트 이름이 넘어오지 않았습니다.'});
    if(!params.hasOwnProperty('type') || !params['type']) return res.status(404).json({'result':'fail','massage':'이벤트 타입이 넘어오지 않았습니다.'});
    if(['PAGEVIEW','CLICK'].indexOf(params['type']) === -1)  return res.status(404).json({'result':'fail','massage':'제공하는 이벤트 타입이 아닙니다.'});
    if(params['type'] == 'PAGEVIEW'){
        if(!params.hasOwnProperty('url_pattern') || !params.hasOwnProperty('url_type') || !params.hasOwnProperty('url_ignore')) return res.status(404).json({'result':'fail','massage':'페이지뷰 타입의 데이터 값(url_pattern,url_type,url_ignore)이 넘어오지 않았습니다.'});
    }

    var option = JSON.parse(collectorOptionManager.getOption(optionName));

    if(option.length === 0){
        option = {};
    }else{
        option = option[0];
    }

    if(!option.hasOwnProperty(params['site'])) {
        option[params['site']] = [];
        option[params['site']].push(params);
    }else{
        for(site in option){

            if(site != params['site']) continue;

            var index = 0;
            var petchIndex = [];
            var findOption = option[site].filter(function(d){
                if(d['id'] == params['id']){
                    petchIndex.push(index);
                    return true;
                }
                index++;
            });

            if(findOption.length == 0){
                option[site].push(params);
            }else{
                option[site][petchIndex[0]] = params;
            }
        }
    }

    var result = collectorOptionManager.initOptions(optionName, JSON.stringify([option]));
    return res.status(200).json({'result':'success','massage': '이벤트 데이터 저장이 완료 되었습니다.'});
});


//옵션 삭제
router.delete('/:site/:id', function(req, res){

    var option = JSON.parse(collectorOptionManager.getOption(optionName));

    if(option.length === 0){
        return res.status(404).json({'result':'fail','massage': '이벤트 옵션이 존재하지 않습니다.'});
    }

    option = option[0];

    if(option.hasOwnProperty(req.params.site)){
        for(site in option){

            if(site != req.params.site) continue;

            var index = 0;
            var petchIndex = [];
            var findOption = option[site].filter(function(d){
                if(d['id'] == req.params.id){
                    petchIndex.push(index);
                    return true;
                }
                index++;
            });

            if(findOption.length > 0){
                option[site].splice(petchIndex[0],1);

                if(option[site].length === 0 ){
                    delete option[site];
                }
            }
        }
    }

    var result = collectorOptionManager.initOptions(optionName, JSON.stringify([option]));
    return res.json({'result':'success','massage': '삭제가 완료되었습니다.'});
});

module.exports = router;
